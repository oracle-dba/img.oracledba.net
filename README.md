# [img.oracledba.net](https://img.oracledba.net) source codes

<br/>

### Run img.oracledba.net on localhost

    # vi /etc/systemd/system/img.oracledba.net.service

Insert code from img.oracledba.net.service

    # systemctl enable img.oracledba.net.service
    # systemctl start img.oracledba.net.service
    # systemctl status img.oracledba.net.service

http://localhost:4068
